// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import Axios from 'axios'
import { i18n } from './lang/config'
import { MdButton, MdContent, MdTabs, MdCheckbox, MdDialog, MdProgress, MdField, MdAutocomplete, MdMenu, MdList, MdTooltip, MdToolbar, MdDrawer } from 'vue-material/dist/components'

Vue.prototype.$axios = Axios

Vue.config.productionTip = false

Vue.use(MdButton)
Vue.use(MdContent)
Vue.use(MdTabs)
Vue.use(MdCheckbox)
Vue.use(MdDialog)
Vue.use(MdProgress)
Vue.use(MdField)
Vue.use(MdAutocomplete)
Vue.use(MdMenu)
Vue.use(MdList)
Vue.use(MdTooltip)
Vue.use(MdToolbar)
Vue.use(MdDrawer)

let deviceMobile = false
if (window.innerWidth <= 768) {
  deviceMobile = true
}

Vue.prototype.$deviceMobile = deviceMobile

/* eslint-disable no-new */
new Vue({
  el: '#app',
  i18n,
  components: { App },
  template: '<App/>'
})
