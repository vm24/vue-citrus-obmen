import Vue from 'vue'
import VueI18n from 'vue-i18n'
import axios from 'axios'

Vue.use(VueI18n)

export const i18n = new VueI18n({
  locale: 'ru',
  fallbackLocale: 'ru',
  silentTranslationWarn: true,
  messages: {}
})

const loadedLanguages = []

function setI18nLanguage (lang) {
  i18n.locale = lang
  axios.defaults.headers.common['Accept-Language'] = lang
  document.querySelector('html').setAttribute('lang', lang)
  return lang
}

export function loadLanguageAsync (lang) {
  if (!loadedLanguages.includes(lang)) {
    let message = {}
    axios.get(`https://my.citrus.ua/${lang}/api/i18/tradein`).then((response) => {
      for (let item in response.data) {
        let key = item.replace('tradein::', '')
        key = key.split('.')
        if (message[key[0]]) {
          message[key[0]][key[1]] = response.data[item]
        } else {
          message[key[0]] = {}
          message[key[0]][key[1]] = response.data[item]
        }
      }
      i18n.setLocaleMessage(lang, message)
      loadedLanguages.push(lang)
      return setI18nLanguage(lang)
    })
  }
  return Promise.resolve(setI18nLanguage(lang))
}
